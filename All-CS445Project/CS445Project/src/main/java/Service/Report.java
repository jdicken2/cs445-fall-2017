package Service;

public class Report 
{
	public int reportID;
	public String name;
	public int totalShows;
	public int totalSeats;
	public String overallOccupanvy;
	
	public int getReportID() {
		return reportID;
	}
	public String getName() {
		return name;
	}
	public int getTotalShows() {
		return totalShows;
	}
	public int getTotalSeats() {
		return totalSeats;
	}
	public String getOverallOccupanvy() {
		return overallOccupanvy;
	}
	public void setReportID(int reportID) {
		this.reportID = reportID;
	}
	public void setName(String name) {
		this.name = name;
	}
	public void setTotalShows(int totalShows) {
		this.totalShows = totalShows;
	}
	public void setTotalSeats(int totalSeats) {
		this.totalSeats = totalSeats;
	}
	public void setOverallOccupanvy(String overallOccupanvy) {
		this.overallOccupanvy = overallOccupanvy;
	}
	
	
}
