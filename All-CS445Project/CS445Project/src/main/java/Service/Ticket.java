package Service;

public class Ticket 
{
	public int tid;
	public int price;
	public String status;
	
	//Patron
	public String name;
	public String phoneNumber;
	public String email;
	public String billingAddress;
	public double ccNumber;
	public String creditCardNumber;
	
	public int getTid() {
		return tid;
	}
	public int getPrice() {
		return price;
	}
	public String getStatus() {
		return status;
	}
	public String getName() {
		return name;
	}
	public String getPhoneNumber() {
		return phoneNumber;
	}
	public String getEmail() {
		return email;
	}
	public String getBillingAddress() {
		return billingAddress;
	}
	public double getCcNumber() {
		return ccNumber;
	}
	public String getCreditCardNumber() {
		return creditCardNumber;
	}
	public void setTid(int tid) {
		this.tid = tid;
	}
	public void setPrice(int price) {
		this.price = price;
	}
	public void setStatus(String status) {
		this.status = status;
	}
	public void setName(String name) {
		this.name = name;
	}
	public void setPhoneNumber(String phoneNumber) {
		this.phoneNumber = phoneNumber;
	}
	public void setEmail(String email) {
		this.email = email;
	}
	public void setBillingAddress(String billingAddress) {
		this.billingAddress = billingAddress;
	}
	public void setCcNumber(double ccNumber) {
		this.ccNumber = ccNumber;
	}
	public void setCreditCardNumber(String creditCardNumber) {
		this.creditCardNumber = creditCardNumber;
	}
	
	
	
}
