package Service;

public class Order 
{
	public int orderID;
	public String dateOrdered;
	
	public int orderAmount;
	public int numberTickets;
	public String orderedBy;
	
	public int getOrderID() {
		return orderID;
	}
	public String getDateOrdered() {
		return dateOrdered;
	}
	public int getOrderAmount() {
		return orderAmount;
	}
	public int getNumberTickets() {
		return numberTickets;
	}
	public String getOrderedBy() {
		return orderedBy;
	}
	public void setOrderID(int orderID) {
		this.orderID = orderID;
	}
	public void setDateOrdered(String dateOrdered) {
		this.dateOrdered = dateOrdered;
	}
	public void setOrderAmount(int orderAmount) {
		this.orderAmount = orderAmount;
	}
	public void setNumberTickets(int numberTickets) {
		this.numberTickets = numberTickets;
	}
	public void setOrderedBy(String orderedBy) {
		this.orderedBy = orderedBy;
	}
	
	
	
}
