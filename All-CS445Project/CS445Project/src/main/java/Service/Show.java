package Service;


public class Show 
{
	public String name;
	public String web;
	public String date;
	public String time;
	public int showID;
	
	public Show()
	{
		name="none";
		web="none";
		date="none";
		time="0:00";
		showID=0;
	}
	
	public Show(String na, String we, String da, String ti, int sID)
	{
		name=na;
		web=we;
		date=da;
		time=ti;
		showID=sID;
	}
	
	public String getName()
	{
		return name;
	}
	
	public String getWeb()
	{
		return web;
	}
	
	public String getDate()
	{
		return date;
	}
	
	public String getTime()
	{
		return time;	
	}
	
	public int getID()
	{
		return showID;
	}
	
	public void setName(String na)
	{
		name=na;
	}
	
	public void setWeb(String we)
	{
		web=we;
	}
	
	public void setTime(String ti)
	{
		time=ti;
	}
	
	public void setID(int ID)
	{
		showID=ID;
	}
}
